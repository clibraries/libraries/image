#include "image.hpp"

#include <iostream>

namespace _utils{

    template <typename T> T min(T a, T b){
        return a < b ? a : b;
    }
    template <typename T> T max(T a, T b){
        return a > b ? a : b;
    }

    void clearScreen(){
        printf("\033[2J");
    }

    void set_color(std::ostream& os, unsigned char r, unsigned char g, unsigned char b){
        os << "\033[48;2;" << (int)r << ";" << (int)g << ";" << (int)b << "m";
    }

    void skip_line(std::istream &is){
        char c;
        do{
            c = is.get();
        }while(c != '\n');
    }
    void skip_comments(std::istream &is){
        char c;
        do{
            c = is.get();
            if(c == '#'){
                skip_line(is);
            }
        }while(c == '#' || c == '\n' || c == ' ');
        is.putback(c);
    }
}

using namespace _utils;

namespace Image{
    //GrayImage
        GrayImage::GrayImage(uint16_t width, uint16_t height):
            Image<uint8_t>(width, height){
        }
        GrayImage::~GrayImage(){
        }

        //open a PGM file and return a GrayImage
        GrayImage* GrayImage::readPGM(std::istream& is){
            
            std::string magic_number;
        _utils::skip_comments(is);
        is >> magic_number;
        if(magic_number == "P5"){
            return GrayImage::readPGMP5(is);
        }
        else if(magic_number == "P2"){
            return GrayImage::readPGMP2(is);
        }
        else{
            throw std::runtime_error("Invalid magic number (got " + magic_number + ")");
        }
    }

    GrayImage* GrayImage::readPGMP5(std::istream &is){
        //note that the magic number has already been read
        skip_comments(is);
        uint64_t width, height;
        is >> width >> height;
        skip_comments(is);
        uint64_t maxval; //I use a uint64_t because here we don't know yet if maxval will be 255 or not
        is >> maxval;
        if(maxval != 255){
            throw std::runtime_error("Maxval must be 255 (got " + std::to_string(maxval) + ")");
        }
        GrayImage* img = new GrayImage(width, height);
        skip_comments(is);
        is.read((char*)img->array_, width*height); //note that the cast is necessary because _data is a uint8_t*, but read() expects a char*
        if(is.fail()){
            throw std::runtime_error("Error while reading the file encoded in PGM P5");
        }
        return img;
    }

    GrayImage* GrayImage::readPGMP2(std::istream &is){
        //note that the magic number has already been read
        _utils::skip_comments(is);
        uint64_t width, height;
        is >> width >> height;
        _utils::skip_comments(is);
        uint64_t maxval;
        is >> maxval;
        if(maxval != 255){
            throw std::runtime_error("Maxval must be 255 (got " + std::to_string(maxval) + ")");
        }
        GrayImage* img = new GrayImage(width, height);
        _utils::skip_comments(is);
        for(uint64_t i = 0; i < width*height; i++){
            uint64_t value;
            is >> value;
            if (is.fail()){
                throw std::runtime_error("Error while reading the file encoded in PGM P2");
            }
            if(value > maxval){
                throw std::runtime_error("Pixel value must be <= maxval (got " + std::to_string(value) + ")");
            }
            img->array_[i] = value;
        }
        return img;
    }

        //save the GrayImage in a PGM file
        void GrayImage::writePGM(std::ostream& os) const {
            os << "P5" << std::endl;
            os << "#Image sauvegardée par Antoine Buirey pour les TP de RepCod." << std::endl;
            os << width_ << " " << height_ << std::endl;
            os << "255" << std::endl;
            os.write((char*)(array_), getWidth()*getHeight());
        }


        //convert a ColorImage to a GrayImage
        GrayImage* GrayImage::fromColorImage(const ColorImage& colorImage){
            GrayImage* grayImage = new GrayImage(colorImage.getWidth(), colorImage.getHeight());
            for(uint16_t y = 0; y < colorImage.getHeight(); ++y){
                for(uint16_t x = 0; x < colorImage.getWidth(); ++x){
                    grayImage->pixel(x, y) = (double)colorImage.pixel(x, y).r * 0.299 + (double)colorImage.pixel(x, y).g * 0.587 + (double)colorImage.pixel(x, y).b * 0.114;
                }
            }
            return grayImage;
        }

        //convert a GrayImage to a ColorImage
        ColorImage* GrayImage::toColorImage() const{
            return ColorImage::fromGrayImage(*this);
        }


        void GrayImage::show(std::ostream& stream) const {
            const char* PIX = "  ";

            for(uint16_t y = 0; y < getHeight(); ++y){
                for (uint16_t x = 0; x < getWidth(); ++x){
                    set_color(stream, this->pixel(x, y), this->pixel(x, y), this->pixel(x, y));
                    stream << PIX << std::flush;
                }
                stream << "\033[0m"; //reset the color
                stream << std::endl; //flush the stream and go to the next line
            }
        }

        void GrayImage::asciiart(std::ostream& stream) const{
            const char* PIX = " .:-=+*#%@";
            for(uint16_t y = 0; y < getHeight(); ++y){
                for (uint16_t x = 0; x < getWidth(); ++x){
                    stream << PIX[this->pixel(x, y) / 25] << PIX[this->pixel(x, y) / 25] << std::flush;
                }
                stream << std::endl;
            }
        }

    //Color
        Color::Color():
            r(0), g(0), b(0){
        }
        Color::Color(uint8_t _r, uint8_t _g, uint8_t _b):
            r(_r), g(_g), b(_b){
        }
        Color::~Color(){
        }

        Color operator+(const Color a, const Color b){
            return Color(
                std::min((uint8_t)(a.r + b.r), (uint8_t)255),
                std::min((uint8_t)(a.g + b.g), (uint8_t)255),
                std::min((uint8_t)(a.b + b.b), (uint8_t)255)
            );
        }

        Color operator*(double coef, Color a){
            return Color(
                std::min((uint8_t)(coef * a.r), (uint8_t)255),
                std::min((uint8_t)(coef * a.g), (uint8_t)255),
                std::min((uint8_t)(coef * a.b), (uint8_t)255)
            );
        }



    //ColorImage
        ColorImage::ColorImage(uint16_t width, uint16_t height):
            Image<Color>(width, height){
        }
        ColorImage::~ColorImage(){
        }

        //open a PPM file and return a ColorImage
        ColorImage* ColorImage::readPPM(std::istream& is){
            std::string format;
            uint16_t width, height, max_value;
            is >> format >> width >> height >> max_value;
            if (format != "P6"){
                throw std::runtime_error("Invalid file format");
            }
            if (max_value != 255){
                throw std::runtime_error("Invalid max value");
            }

            //we need to skip the space between the max value and the data
            is.ignore(1);

            ColorImage* image = new ColorImage(width, height);
            is.read((char*)(image->array_), width*height*sizeof(Color));
            return image;
        }

        //save the ColorImage in a PPM file
        void ColorImage::writePPM(std::ostream& os) const {
            os << "P6" << ' ' << getWidth() << ' ' << getHeight() << ' ' << "255" << ' ';
            os.write(reinterpret_cast<char*>(array_), getWidth()*getHeight()*sizeof(Color));
        }

        //convert a GrayImage to a ColorImage
        ColorImage* ColorImage::fromGrayImage(const GrayImage& grayImage){
            ColorImage* colorImage = new ColorImage(grayImage.getWidth(), grayImage.getHeight());
            for(uint16_t y = 0; y < grayImage.getHeight(); ++y){
                for(uint16_t x = 0; x < grayImage.getWidth(); ++x){
                    colorImage->pixel(x, y) = Color(grayImage.pixel(x, y), grayImage.pixel(x, y), grayImage.pixel(x, y));
                }
            }
            return colorImage;
        }
        GrayImage* ColorImage::toGrayImage() const{
            return GrayImage::fromColorImage(*this);
        }

        void ColorImage::show(std::ostream& stream) const {
            const char* PIX = "  ";

            for(uint16_t y = 0; y < getHeight(); ++y){
                for (uint16_t x = 0; x < getWidth(); ++x){
                    set_color(stream, this->pixel(x, y).r, this->pixel(x, y).g, this->pixel(x, y).b);
                    stream << PIX << std::flush;
                }
                stream << "\033[0m"; //reset the color
                stream << std::endl; //flush the stream to show the image line by line
            }
        }

        void ColorImage::asciiart(std::ostream& stream) const{
            const char* PIX = " .:-=+*#%@";
            for(uint16_t y = 0; y < getHeight(); ++y){
                for (uint16_t x = 0; x < getWidth(); ++x){
                    stream << PIX[(this->pixel(x, y).r + this->pixel(x, y).g + this->pixel(x, y).b) / 75] << PIX[(this->pixel(x, y).r + this->pixel(x, y).g + this->pixel(x, y).b) / 75] << std::flush;
                }
                stream << std::endl;
            }
        }

        ColorImage* ColorImage::getChannel(CHANNEL channel) const{
            ColorImage* colorImage = new ColorImage(getWidth(), getHeight());
            for(uint16_t y = 0; y < getHeight(); ++y){
                for(uint16_t x = 0; x < getWidth(); ++x){
                    switch(channel){
                        case CHANNEL::RED:
                            colorImage->pixel(x, y) = Color(pixel(x, y).r, 0, 0);
                            break;
                        case CHANNEL::GREEN:
                            colorImage->pixel(x, y) = Color(0, pixel(x, y).g, 0);
                            break;
                        case CHANNEL::BLUE:
                            colorImage->pixel(x, y) = Color(0, 0, pixel(x, y).b);
                            break;
                    }
                }
            }
            return colorImage;
        }

        ColorImage* operator+(const ColorImage& img1, const ColorImage& img2){
            if(img1.getWidth() != img2.getWidth() || img1.getHeight() != img2.getHeight()){
                throw std::runtime_error("Images must have the same size");
            }

            ColorImage* newColorImage = new ColorImage(img1.getWidth(), img2.getHeight());
            for(uint16_t y = 0; y < img1.getHeight(); ++y){
                for(uint16_t x = 0; x < img1.getWidth(); ++x){
                    newColorImage->pixel(x, y) = Color( min((uint8_t)(img1.pixel(x, y).r + img2.pixel(x, y).r), (uint8_t)255),
                                                        min((uint8_t)(img1.pixel(x, y).g + img2.pixel(x, y).g), (uint8_t)255),
                                                        min((uint8_t)(img1.pixel(x, y).b + img2.pixel(x, y).b), (uint8_t)255));
                }
            }
            return newColorImage;
        }

        ColorImage* operator+=(ColorImage& img1, const ColorImage& img2){
            if(img1.getWidth() != img2.getWidth() || img1.getHeight() != img2.getHeight()){
                throw std::runtime_error("Images must have the same size");
            }

            for(uint16_t y = 0; y < img1.getHeight(); ++y){
                for(uint16_t x = 0; x < img1.getWidth(); ++x){
                    img1.pixel(x, y) = Color(min((uint8_t)(img1.pixel(x, y).r + img2.pixel(x, y).r), (uint8_t)255),
                                        min((uint8_t)(img1.pixel(x, y).g + img2.pixel(x, y).g), (uint8_t)255),
                                        min((uint8_t)(img1.pixel(x, y).b + img2.pixel(x, y).b), (uint8_t)255));
                }
            }
            return &img1;
        }
}