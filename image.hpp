#ifndef __IMAGE_HPP__
#define __IMAGE_HPP__

#include <cstdint>

#include <fstream>


//This is a list of all classes in  the image library
namespace Image{
    template <typename PixelType> class Image;
    class GrayImage;
    class ColorImage;
    class Color;
    enum class CHANNEL;
    class ColorChannel;
}

namespace Image{
    template <typename PixelType> class Image{
        protected:
            uint16_t width_;
            uint16_t height_;
            PixelType* array_;

        public:
            Image() = delete; //it makes no sense to have an image with no size
            Image(uint16_t width, uint16_t height):
                width_(width), height_(height), array_(nullptr){
                array_ = new PixelType[width_*height_];
            }
            Image(const Image<PixelType>& orig):
                width_(orig.width_), height_(orig.height_), array_(nullptr){
                array_ = new PixelType[width_*height_];
                std::copy(orig.array_, orig.array_ + width_*height_, array_);
            }
            ~Image(){
                delete[] array_;
            }

            const uint16_t& getWidth() const{return width_;}
            const uint16_t& getHeight() const{return height_;}
            PixelType& pixel(uint16_t x, uint16_t y){return array_[y*width_ + x];}
            const PixelType& pixel(uint16_t x, uint16_t y) const{return array_[y*width_ + x];}

            virtual void clear(PixelType defaultValue = 0){
                    std::fill(array_, array_ + width_*height_, defaultValue);
                }

                void rectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, PixelType value){
                if(x + w > width_ || y + h > height_){
                    throw std::out_of_range("Rectangle out of range");
                }

                for(uint16_t i = x; i < x + w; i++){
                    pixel(i, y) = value;
                    pixel(i, y + h - 1) = value;
                }
                for(uint16_t j = y; j < y + h; j++){
                    pixel(x, j) = value;
                    pixel(x + w - 1, j) = value;
                }
            }

            void fillRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, PixelType value){
                if(x + w > width_ || y + h > height_){
                    throw std::out_of_range("Rectangle out of range");
                }

                for(uint16_t i = x; i < x + w; i++){
                    for(uint16_t j = y; j < y + h; j++){
                        pixel(i, j) = value;
                    }
                }
            }

            Image<PixelType>* simpleScale(uint16_t w, uint16_t h) const;                    //? what is this ?
            Image<PixelType>* billinearScale(uint16_t w, uint16_t h) const;                 //? what is this ?

            virtual void line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, PixelType value){
                if(x1 >= width_ || x2 >= width_ || y1 >= height_ || y2 >= height_){
                    throw std::out_of_range("Line out of range");
                }

                int16_t dx = x2 - x1;
                int16_t dy = y2 - y1;
                int16_t x = x1;
                int16_t y = y1;

                //(x,y) is the first pixel, in absolute coordinates
                //(dx,dy) is the vector from (x1,y1) to (x2,y2)

                if(dx == 0){ //this mean that the line is vertical, so it's a simple case
                    if(y1 > y2){
                        std::swap(y1, y2);
                    }
                    for(uint16_t j = y1; j <= y2; j++){
                        pixel(x, j) = value;
                    }
                    return;
                }
                if(dy == 0){ //same as above, but for horizontal lines
                    if(x1 > x2){
                        std::swap(x1, x2);
                    }
                    for(uint16_t i = x1; i <= x2; i++){
                        pixel(i, y) = value;
                    }
                    return;
                }

                if(abs(dx) > abs(dy)){ // I split it into 2 cases for speed optimization
                    if(x1 > x2){
                        std::swap(x1, x2);
                        std::swap(y1, y2);
                    }
                    double a = (double)dy / (double)dx;
                    double b = y1 - a * x1;
                    for(uint16_t i = x1; i <= x2; i++){
                        pixel(i, (uint16_t)(a * i + b)) = value;
                    }
                }
                else{
                    if(y1 > y2){
                        std::swap(x1, x2);
                        std::swap(y1, y2);
                    }
                    double a = (double)dx / (double)dy;
                    double b = x1 - a * y1;
                    for(uint16_t j = y1; j <= y2; j++){
                        pixel((uint16_t)(a * j + b), j) = value;
                    }
                }
            }

            virtual void show(std::ostream& stream) const = 0;                              //HERE this fuction is not implemented in the scheme, but allow to print the image to a stream, useful for debugging
            virtual void asciiart(std::ostream& stream) const = 0;                          //HERE this fuction is not implemented in the scheme, but allow to print the image to a stream, useful for debugging


            virtual void resize(uint16_t width, uint16_t height){
            if(width > getWidth() || height > getHeight()){ //this will be implemented later
                throw std::runtime_error("The new size must be smaller than the original size");
            }
            else if(width == getWidth() && height == getHeight()){
                return; //I don't throw an error because it's not an error, it's just useless to do something
            }
            else if(width == 0 || height == 0){
                throw std::runtime_error("The new size must be greater than 0");
            }

            PixelType* newArray = new PixelType[width*height];
                for(uint16_t y = 0; y < height; ++y){
                    for(uint16_t x = 0; x < width; ++x){
                        newArray[y*width + x] = pixel(width_*x/width, height_*y/height);
                    }
                }
                delete[] array_;
                array_ = newArray;
                width_ = width;
                height_ = height;
            }

            friend inline std::ostream& operator<<(std::ostream& stream, const Image<PixelType>& image){
                image.show(stream);
                return stream;
            }
    };


    class GrayImage : public Image<uint8_t>{
        public:
            GrayImage() = delete;
            GrayImage(uint16_t width, uint16_t height);
            ~GrayImage();
            
            void writePGM(std::ostream& os) const;
            static GrayImage* readPGM(std::istream& is);

            static GrayImage* readPGMP5(std::istream& is);
            static GrayImage* readPGMP2(std::istream& is);

            static GrayImage* fromColorImage(const ColorImage& colorImage);
            ColorImage* toColorImage() const;

            virtual void show(std::ostream& stream) const override;                     //HERE this fuction is not implemented in the scheme, but allow to print the image to a stream, useful for debugging
            virtual void asciiart(std::ostream& stream) const override;                 //HERE this fuction is not implemented in the scheme, but allow to print the image to a stream, useful for debugging

            inline void resize(uint16_t w, uint16_t h){
                return Image<uint8_t>::resize(w, h);
            }
            inline GrayImage* simpleScale(uint16_t w, uint16_t h) const{
                return (GrayImage*)Image<uint8_t>::simpleScale(w, h);
            }
            inline GrayImage* billinearScale(uint16_t w, uint16_t h) const{
                return (GrayImage*)Image<uint8_t>::billinearScale(w, h);
            }
            inline void line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t value){
                Image<uint8_t>::line(x1, y1, x2, y2, value);
            }
            inline void rectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint8_t value){
                Image<uint8_t>::rectangle(x, y, w, h, value);
            }
            inline void fillRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint8_t value){
                Image<uint8_t>::fillRectangle(x, y, w, h, value);
            }


    };



    class Color{
        public:
            uint8_t r;
            uint8_t g;
            uint8_t b;
        
        public:
            Color(); //I need this for the array in the image
            Color(uint8_t r, uint8_t g, uint8_t b);
            ~Color();

            friend Color operator+(const Color a, const Color b);
            friend Color operator*(double coef, Color a);
    };

    //this is a list of primary, secondary colors, grayscale and black&white
    namespace COLORS{
        //b&w
        const Color BLACK = Color(0, 0, 0); //(0, 0, 0)
        const Color WHITE = Color(255, 255, 255); //(255, 255, 255)

        //primary
        const Color RED = Color(255, 0, 0); //(255, 0, 0)
        const Color GREEN = Color(0, 255, 0); //(0, 255, 0)
        const Color BLUE = Color(0, 0, 255); //(0, 0, 255)

        //secondary
        const Color CYAN = Color(0, 255, 255); //(0, 255, 255)
        const Color MAGENTA = Color(255, 0, 255); //(255, 0, 255)
        const Color YELLOW = Color(255, 255, 0); //(255, 255, 0)

        //grayscale (from 0 to 16)
        const Color GRAY_0 = Color(0, 0, 0); //black, but I want it in the grayscale list (0, 0, 0)
        const Color GRAY_1 = Color(16, 16, 16); //(16, 16, 16)
        const Color GRAY_2 = Color(32, 32, 32); //(32, 32, 32)
        const Color GRAY_3 = Color(48, 48, 48); //(48, 48, 48)
        const Color GRAY_4 = Color(64, 64, 64); //(64, 64, 64)
        const Color GRAY_5 = Color(80, 80, 80); //(80, 80, 80)
        const Color GRAY_6 = Color(96, 96, 96); //(96, 96, 96)
        const Color GRAY_7 = Color(112, 112, 112); //(112, 112, 112)
        const Color GRAY_8 = Color(128, 128, 128); //(128, 128, 128)
        const Color GRAY_9 = Color(144, 144, 144); //(144, 144, 144)
        const Color GRAY_10 = Color(160, 160, 160); //(160, 160, 160)
        const Color GRAY_11 = Color(176, 176, 176); //(176, 176, 176)
        const Color GRAY_12 = Color(192, 192, 192); //(192, 192, 192)
        const Color GRAY_13 = Color(208, 208, 208); //(208, 208, 208)
        const Color GRAY_14 = Color(224, 224, 224); //(224, 224, 224)
        const Color GRAY_15 = Color(240, 240, 240); //(240, 240, 240)
        const Color GRAY_16 = Color(255, 255, 255); //white, but I want it in the grayscale list (255, 255, 255)
    }

    enum class CHANNEL{
        RED,
        GREEN,
        BLUE
    };

    class ColorImage : public Image<Color>{ 
        public:

            ColorImage() = delete;
            ColorImage(uint16_t width, uint16_t height);
            ~ColorImage();

            void writePPM(std::ostream& os) const;
            static ColorImage* readPPM(std::istream& is);
            void writeTGA(std::ostream& os) const;                                          //? what is this ?
            static ColorImage* readTGA(std::istream& is);                                   //? what is this ?

            ColorImage* simpleScale(uint16_t w, uint16_t h) const;                          //? what is this ?
            ColorImage* billinearScale(uint16_t w, uint16_t h) const;                       //? what is this ?

            void writeJPEG(std::ostream& os) const;                                         //... I am lazy to implement this for now
            static ColorImage* readJPEG(std::istream& is);                                  //... I am lazy to implement this for now

            static ColorImage* fromGrayImage(const GrayImage& grayImage);
            GrayImage* toGrayImage() const;

            virtual void show(std::ostream& stream) const override;                         //HERE this fuction is not implemented in the scheme, but allow to print the image to a stream, useful for debugging (and it's fun !)
            virtual void asciiart(std::ostream& stream) const override;                     //HERE this fuction is not implemented in the scheme, but allow to print the image to a stream, useful for debugging (and it's fun !)

            ColorImage* getChannel(CHANNEL channel) const;

            friend ColorImage* operator+(const ColorImage& img1, const ColorImage& img2);
            friend ColorImage* operator+=(ColorImage& img1, const ColorImage& img2);

            inline void line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, Color value){
                Image<Color>::line(x1, y1, x2, y2, value);
            }
            inline void rectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, Color value){
                Image<Color>::rectangle(x, y, w, h, value);
            }
            inline void fillRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, Color value){
                Image<Color>::fillRectangle(x, y, w, h, value);
            }
            inline void resize(uint16_t w, uint16_t h){ 
                return Image<Color>::resize(w, h);
            }
            inline void clear(Color defaultValue = COLORS::BLACK){
                return Image<Color>::clear(defaultValue);
            }
            inline Color& pixel(uint16_t x, uint16_t y){
                return Image<Color>::pixel(x, y);
            }
            inline const Color& pixel(uint16_t x, uint16_t y) const{
                return Image<Color>::pixel(x, y);
            }
    };
}



#endif // __IMAGE_HPP__