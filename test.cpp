#include "image.hpp"

#include <iostream>


int main(){  
    std::ios::sync_with_stdio(false); //disable synchronization between C and C++ standard streams to speed up printing of images

    std::ifstream color_file("lena.ppm", std::ios::binary);

    Image::ColorImage* color_image = Image::ColorImage::readPPM(color_file);
    Image::GrayImage* gray_image = color_image->toGrayImage();

    gray_image->resize(32, 32);
    color_image->resize(32, 32);

    gray_image->asciiart(std::cout);
    std::cout << *color_image << std::endl;

    // Image::ColorImage gradient = Image::ColorImage(256, 256); 
    // for(uint16_t i = 0; i < 256; i++){
    //     for(uint16_t j = 0; j < 256; j++){
    //         gradient.pixel(i, j) = Image::Color(i, j, 0);
    //     }
    // }
    //std::cout << gradient << std::endl;

}